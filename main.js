var container = $('.calculator-wr'); // контейнер с кнопками
var input = $('.input-calc'); // инпут
var numbers = '123456789';
var operator = ''; // хранит выбранный оператоп
var storedValue = ''; // хранит число после нажатия на одного из операторов /+-*
var val = ''; // текущее значение из инпута
var enabled = true; // если не Infinity или NaN
var memory = 0; // для работы с памятью

container.on('click', function(e) {
    var target = '';

    // если была кликнут элемент с классом btn
    if (e.target.classList.contains('btn')) {
        target = e.target.innerHTML; // текст внутри элемента
    }

    if (enabled) {

        // если текст равен одной из цифра 1-9
        if (numbers.indexOf(target) !== -1){
            // если в инпуте только 0 тогда заменяеться на цифру иначе прибавляеться
            if (input.val() === '0'){
                input.val(target);
            }else {
                input.val(input.val() + target);
            }
            val = input.val();
        }

        if (target === '0') {
            // если в инпуте только 0 тогда 0.0 иначе прибавляеться 0
            if (input.val() === '0') {
                input.val(input.val() + '.' + target);
            }else {
                input.val(input.val() + target);
            }
            val = input.val();
        }

        // если текст . и инпут не содержит .
        if (target ===  '.' && input.val().indexOf('.') === -1) {
            input.val(input.val() + target);
            val = input.val();
        }

        if (target === '+-') {
            input.val(-input.val());
            val = input.val();
        }

        // если текст <- и длинна текста в инпуте больше 0
        if (target === '&lt;-' && input.val().length > 0) {
            input.val(input.val().slice(0, -1));
            val = input.val();
        }

        // если текст инпута не равен 0
        if (target === '1/X' && input.val() !== '0') {
            input.val(1 / input.val());
            val = input.val();
        }

        if (target === 'X^2') {
            input.val(Math.pow(input.val(), 2));
            val = input.val();
        }

        if (target === '√') {
            input.val(Math.sqrt(input.val()));
            val = input.val();
        }

        if (target === '+') {
            operator = '+';
            storedValue = input.val();
            input.val('');
            val = input.val();
        }

        if (target === '-') {
            operator = '-';
            storedValue = input.val();
            input.val('');
            val = input.val();
        }

        if (target === '*') {
            operator = '*';
            storedValue = input.val();
            input.val('');
            val = input.val();
        }

        if (target === '/') {
            operator = '/';
            storedValue = input.val();
            input.val('');
            val = input.val();
        }

        // если текст % и оператор один из *+-/
        if (target === '%' && operator !== '') {
            if (input.val() === '') {
                input.val(Number(storedValue)/100 * storedValue);
            }else {
                input.val(Number(storedValue)/100 * Number(input.val()));
            }

            val = input.val();
        }

        if (target === 'MC') {
            memory = 0;
        }

        if (target === 'MR') {
            input.val(memory)
        }

        if (target === 'M+') {
            memory += Number(input.val());
        }

        if (target === 'M-') {
            memory -= Number(input.val());
        }

        if (target === 'MS') {
            memory = Number(input.val());
        }

        if (target === '=' && operator !== '') {
            if (operator === '+') storedValue = Number(storedValue) + Number(val);
            if (operator === '-') storedValue = Number(storedValue) - Number(val);
            if (operator === '*') storedValue = Number(storedValue) * Number(val);
            if (operator === '/') storedValue = Number(storedValue) / Number(val);

            input.val(storedValue)
        }

        // если в инпуте Infinity или NaN тогда на нельзя нажимать на кнопки, кроме C или CE
        if (input.val() === 'Infinity' || input.val() === 'NaN') {
            if (input.val() === 'NaN') {
                input.val('Error')
            }
            enabled = false;
        }
    }

    if (target === 'CE') {
        input.val('');
        val = '';
        enabled = true;
    }

    if (target === 'C') {
        input.val('');
        val = '';
        storedValue = '';
        operator = '';
        enabled = true;
    }
});